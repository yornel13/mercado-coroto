import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    textToSearch: string;

    constructor(private router: Router) {
    }

    ngOnInit(): void {
    }

    search() {
        console.log(this.textToSearch);
        if (!this.textToSearch) {
            return;
        }
        this.router.navigate(['/search', this.textToSearch]);
    }

}
