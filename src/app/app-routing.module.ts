import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { SearchComponent } from './pages/search/search.component';
import { StoresComponent } from './pages/stores/stores.component';
import { FaqComponent } from './pages/faq/faq.component';


const routes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'search/:text', component: SearchComponent},
    {path: 'stores', component: StoresComponent},
    {path: 'faq', component: FaqComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
