import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Product } from './product';

@Injectable({
    providedIn: 'root'
})
export class ProductsService {

    private itemsCollection: AngularFirestoreCollection<Product>;
    products: any[] = [];

    constructor(private angularFirestore: AngularFirestore) {
    }

    getProducts() {
        this.itemsCollection = this.angularFirestore.collection<Product>('products', ref => ref.limit(12));

        return this.itemsCollection.valueChanges()
            .pipe(map(products => {
                for (const prod of products) {
                    this.products.unshift(prod);
                }
                console.log(this.products);
                return products;
            }));
    }
}
