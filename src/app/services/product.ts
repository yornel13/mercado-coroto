export interface Product {
    name: string;
    price: {
        usd?: number;
        bs?: number;
    };
    photoUrl?: string;
    description?: string
    category?: string;
    state?: string;
}
