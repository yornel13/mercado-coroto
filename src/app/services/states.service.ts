import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StatesService {

  constructor(private http: HttpClient) { }

    async getNameStates(): Promise<any[]> {
        const json: any = await this.http.get('assets/states_venezuela.json').toPromise();
        const states: any[] = [];
        for (const state of json) {
            states.push({id: state.id_estado, name: state.estado});
        }
        console.log('-> states', states);
        return states;
    }
}
