import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import { Product } from '../../services/product';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    products: Product[] = [];

    constructor(private _productsService: ProductsService) {
    }

    ngOnInit(): void {
        this._productsService.getProducts()
            .subscribe(response => {
                this.products = response;
                console.log('-> response', response);
            });
    }

}
