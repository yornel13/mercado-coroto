import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import { Product } from '../../services/product';
import { StatesService } from '../../services/states.service';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

    products: Product[] = [];
    states: any[] = [];
    selectedCategory = 4;
    selectedState = 7;

    constructor(private _productsService: ProductsService,
                private _statesService: StatesService) {
    }

    ngOnInit(): void {
        this._statesService.getNameStates()
            .then(states => this.states = states);
        this._productsService.getProducts()
            .subscribe(response => {
                this.products = response;
                console.log('-> response', response);
            });
    }

    selectCategory(number: number) {
        console.log('-> number', number);
        this.selectedCategory = number;
    }

    selectState(number: number) {
        console.log('-> number', number);
        this.selectedState = number;
    }

    viewDetails(card: string) {
        console.log('-> card', card);
    }

}
