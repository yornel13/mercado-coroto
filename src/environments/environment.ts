// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: 'AIzaSyAkJslE6spPEFSDV-hokR_dCSx7rh2OILI',
        authDomain: 'mercado-coroto.firebaseapp.com',
        databaseURL: 'https://mercado-coroto.firebaseio.com',
        projectId: 'mercado-coroto',
        storageBucket: 'mercado-coroto.appspot.com',
        messagingSenderId: '670427936431',
        appId: '1:670427936431:web:decfd0697af20de52a065a',
        measurementId: 'G-3R2L8ELHCC'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
